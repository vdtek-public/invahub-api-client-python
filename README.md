# invahub-api-client-python

Public Python client to interact with InvaHub

## Description
This is a simple tool to facilitate calls to [InvaHub API](www.invahub.com) 
from your Python trading algos.


## Installation
Install with pip

```
pip install invahub_api_client
```


## Usage
Initiate a `InvahubClient` object with a default stream:

```python
from invahub_api_client.client import InvahubClient
from invahub_api_client.constants import SIGNAL_INDICATOR_BULL

client = InvahubClient(
    api_key = "you API KEY from your user settings",
    stream_id = "the stream ID from your stream page"
)

# Your algo code here
signal = # ...

client.post_signal(indicator=SIGNAL_INDICATOR_BULL, extra_data=signal.json())
```

Also, see `examples/` folder


## Development
Create a virtualenv. e.g. with `pyenv`:

```shell
pyenv virtualenv 3.9.7 invahubclientenv
pyenv activate invahubclientenv
```

Activate the virtualenv and install dependencies:

```shell
pip install -U -r requirements-dev.txt
```

To install invahub-api-client to your virtualenv directly from the source dir, 
run on the root dir:

```shell
pip install -e .
```


## Examples
In order to run the examples, you need to install the dev requirements
(specific versions are optional):

```shell
pip install pandas==1.4.2
pip install requests==2.27.0
pip install yfinance==0.1.70
```


## Support
Any bugs, issues or recommendations can be posted to the project's public GitLab repo:

https://gitlab.com/vdtek-public/invahub-api-client-python

## Project status
**Active**. As the InvaHub project is evolving, changes are expected in this library to
accommodate future features
