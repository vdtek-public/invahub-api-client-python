import requests
import urllib.parse

from invahub_api_client import constants


class InvahubClient:
    def __init__(self, api_key, stream_id=None, default_symbol=None, invahub_host=constants.INVAHUB_DEFAULT_API_HOST):
        self.api_key = api_key
        self.stream_id = stream_id
        self.invahub_host = invahub_host
        self.default_symbol = default_symbol

    def post_signal(self, indicator, symbol=None, extra_data=None, publish_immediately=True, stream_id=None):
        if not extra_data:
            extra_data = {}
        if stream_id is None and self.stream_id is None:
            raise ValueError("No stream_id specified, nor a default one was set!")
        if symbol is None and self.default_symbol is not None:
            symbol = self.default_symbol
        payload = {
            "indicator": indicator,
            "stream_id": stream_id,
            "extra_data": extra_data,
            "symbol": symbol,
            "publish_immediately": publish_immediately
        }
        headers = {
            "Authentication": "Token {!s}".format(self.api_key)
        }
        endpoint = urllib.parse.urljoin(self.invahub_host, constants.INVAHUB_MARKET_SIGNAL_POST_ENDPOINT)
        try:
            response = requests.post(endpoint, json=payload, headers=headers)
        except Exception as e:
            raise e
        else:
            return response



