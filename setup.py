import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="invahub-api-client",
    version="0.1.0",
    author="VDTek",
    author_email="evangelos.doukakis@gmail.com",
    description="A simple Python client to make publish requests to InvaHub API (api.invahub.com)",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/vdtek-public/invahub-api-client-python",
    project_urls={
        "Bug Tracker": "https://gitlab.com/vdtek-public/invahub-api-client-python/-/issues",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=setuptools.find_packages(where="src"),
    python_requires=">=3.9",
    install_requires=["requests>=2.27.1"],
)
