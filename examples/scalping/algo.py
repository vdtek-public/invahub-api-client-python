import pandas as pd


class ScalpingAlgo:
    """
    Simple scalping algo for testing.
    Generates buy signals on 20 minute moving average crossover
    """

    def __init__(self, symbol, mva_width=20):
        self.symbol = symbol
        self.mva_width = mva_width

    def run_for_series(self, data_pd, append=False):
        # apply MVA
        mavg = data_pd.Close.rolling(self.mva_width).mean().values
        closes = data_pd.Close.values

        signal = [0] * len(data_pd)
        # find crossovers
        for i in range(self.mva_width, len(data_pd)):
            if closes[i - 2] < mavg[i - 2] and closes[i - 1] > mavg[i - 1]:
                signal[i] = 1
            elif closes[i - 2] > mavg[i - 2] and closes[i - 1] < mavg[i - 1]:
                signal[i] = -1

        # return generated signals
        signal = pd.DataFrame(data={"signal": signal}, index=data_pd.index)
        return signal
