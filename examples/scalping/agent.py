"""
This agent runs a very simple scalping algorithm for a single symbol on minute data

It loads some historic data and runs every minute. If a new signal is generated while the agent is running, it will post
a Market Signal to InvaHub.
Note that old signals from historic data are discarded.

Run from `examples/` dir as:

    $ python scalping.agent

Kill with Ctrl-C

- It is assumed the user has issued a (demo) API key: "ABCinvahub_api_key123"
- It is assumed the user has created a single-symbol Stream which has a (demo) identifier: "56667A"
"""

import yfinance
import time

from examples.scalping.algo import ScalpingAlgo
from invahub_api_client.client import InvahubClient
from invahub_api_client.constants import SIGNAL_INDICATOR_BULL, SIGNAL_INDICATOR_BEAR

if __name__ == "__main__":
    symbol = "UBER"
    api_key = "ABCinvahub_api_key123"
    stream_id = "56667A"
    loop_interval = 60  # sec

    posted_timestamps = []
    invahub_client = InvahubClient(
        api_key=api_key,
        stream_id=stream_id,
        default_symbol=symbol,
        invahub_host="http://127.0.0.1:8000/"       # must add protocol
    )
    algo = ScalpingAlgo(symbol)

    while True:     # kill with ctrl-C
        data = yfinance.download(tickers=symbol, period="1d", interval="1m")
        results = algo.run_for_series(data)
        data.loc[:, "signal"] = results["signal"]       # append signal to data data

        last_entry = data.iloc[-1]
        last_timestamp = data.index[-1]
        print("... Processing", last_timestamp, last_entry["signal"])
        if last_entry["signal"] == 1 and last_timestamp not in posted_timestamps:
            print("Posting BULL signal")
            try:
                invahub_client.post_signal(SIGNAL_INDICATOR_BULL)
            except Exception as e:
                print("Could not port Market Signal: {!s}".format(e))
            posted_timestamps.append(last_timestamp)
        if last_entry["signal"] == -1 and last_timestamp not in posted_timestamps:
            try:
                invahub_client.post_signal(SIGNAL_INDICATOR_BEAR)
            except Exception as e:
                print("Could not port Market Signal: {!s}".format(e))
            posted_timestamps.append(last_timestamp)
        time.sleep(loop_interval)
